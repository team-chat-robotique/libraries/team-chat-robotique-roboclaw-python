Changelog
=========

0.1.0
-----------------------------

- Implement all commands from Basicmicro's [User Manual](https://downloads.basicmicro.com/docs/roboclaw_user_manual.pdf)
- Document all methods with docstrings
- Change some method names to comply with [PEP8](https://peps.python.org/pep-0008) or to make them more explicit ([_Explicit is better than implicit._](https://peps.python.org/pep-0020)).

0.0.1
-----------------------------

- Imported [`roboclaw_python_library`](https://github.com/basicmicro/roboclaw_python_library)
